===============================================================================
SUPER SMASH BROS.   "Special Bonus" Guide
                                                                       by Milen
===============================================================================

 7/19/99  v1.00: Most corrections made.  For this one I've really gotten so 
much e-mail that I cannot acknowledge everyone who sent me a message.  All I 
can do is bow and applaud the unseen players whose skill and ingenuity have 
uncovered all these bonuses (most have been reported by several people anyway).  
There *are* a few unconfirmed bonuses left, so there's still a little stuff 
left to do.
 6/02/99  v0.91: Lots of things confirmed, others corrected.  The mystery of 
Jackpot finally solved!  Also added "notes" section and extended the Strategy 
section to include tips for each level.
 5/31/99  v0.9 : Lots of corrections!  Also strategy on getting a large number 
of bonuses in one game (and thus getting a high bonus count, for the high score 
record).  Added special secret for getting over one million points.
 5/13/99  v0.8 : "About points and high scores" section added, some entries 
rephrased and some more corrections made.
 5/11/99  v0.7 : More bonuses added, previous ones fixed.  A maintenance 
release.
 5/ 9/99  v0.62: Added one new bonus and made a correction.
 5/ 8/99  v0.61: Added two new bonuses.
 5/ 6/99  v0.6 : Corrections made, categories added, bonuses sorted, new 
bonuses added and some points fixed.
 5/ 5/99  v0.5 : First version.


Going for a high score in Super Smash Bros.?  Here's a list of the "Special 
Bonuses" that I, and growing number of other people, have discovered, and how 
you can get them.  A "Special Bonus" occurs when the game decides you've done 
something neat and gives you points after a match in the one player mode "just 
because."  The instruction manual claims that there are more than twenty of 
these; this proves to be an understatement, as we've found more then fifty!

BONUSES GIVEN AFTER ANY BATTLE

Cheap Shot        -99 pts  Overuse one type of move.  This is known to 
sometimes happen on the final boss, Master Hand.

Fighter Stance    100 pts  Pose with the 'L' button just as a match ends.  Your 
character must be in the pose when "GAME SET" appears, so there is an element 
of timing involved.

No Item          1000 pts  Go the whole match without picking up any items.

Throw Down       2000 pts  Finish off your last opponent with a throw move.

Jackpot          3330 pts  Finish the level with the tens and ones digits of 
your damage meter being the same (for example, 11%, 22%, 33%, etc.)  Finishing 
with 0% doesn't count (but you get Full Power anyway, and maybe No Damage).  If 
your damage is in the hundreds it's possible that you have to have ALL the 
digits match, even the hundreds.

Smash Mania      3500 pts  Comes about when all foes are dispatched using 
nothing but power hits (tap+A).  Power hits from items don't spoil this one for 
you, as long as it was a true "smash."

Full Power       5000 pts  Finish the match with 0% damage.  Note that if you 
win without taking any hits you'll get both this and "No Damage," for a total 
of 20000 pts.  If you take damage, then pick up a health item, but the match 
finishes BEFORE your health meter returns to 0%, you do NOT get this bonus.

Smash-less       5000 pts  This bonus is granted either when you finish a match 
without using any power hits.  This one is fairly common.

Special Move     5000 pts  All moves that connect must be "special moves" 
(up+B, down+B).

Judo Master      5000 pts  Seems to happen when you win by using only throws.  
Best to try during the Yoshi, Kirby or Polygon levels.  Note that any bonus 
that occurs from using only one move will also earn the "Single Move" bonus, 
described below.  (A recent report notes that it may be possible to get Judo 
Master and use another move.  Anyone care to experiment with this?)

Mystic           7000 pts  Win the match right after losing a life, but BEFORE 
you return to the board.  This is one that's awfully difficult to earn on 
purpose.  Tends to be a mixed blessing, since losing a life disqualifies you 
from getting further "No Miss" bonuses for that game (see below).

Single Move      8000 pts  Win the battle by only using one move (like DK's 
spin-jump).  You can use that move as often as you want as long as you don't 
use any others.  It also seems that only moves that connect with your enemy  
count.

Shield Breaker   8000 pts  This appears to happen when a shield is broken.  The 
shield must be COMPLETELY broken; you know this has happened when your opponent 
is stunned.  The enemy cannot come out of his block.  The only time I've seen 
this so far is when a Pokeball produced a Staryu, which blew away an enemy's 
shield in short order.  It's very difficult to do this using normal attacks.

Last Second      8000 pts  Win the match with one second left on the clock.  
Note that this means you earn almost no Time Bonus for that round, so it's 
usually better not to wait.  This bonus probably never comes up if you're 
playing with infinite time.

Vegetarian       9000 pts  Apparently, eat at three Maxim Tomatoes in one 
battle.  Seems to happen most often during the Fighting Polygon Team bout.

Lucky 3          9990 pts  Finish the level with exactly 3:33 left on the 
clock.

Speedster       10000 pts  To get this, you must finish the match within thirty 
seconds.  If the clock reads 4:30 when GAME SET appears you get the points.  If 
it reads 4:29 you're out of luck.

Star Finish     10000 pts  On a board on which you have only one opponent,  
finish him off with the "disappears off into the distance" effect (or falls 
down in the foreground) instead of the off-the-bottom or off-the-side 
explosions.

Butterfly       10000 pts  Unconfirmed.  Defeat your opponent (with a physical 
attack?) when he's out over the edge and trying to get back on the board.  Or 
maybe you have to hit him while he's hanging over the edge?  From the Smash 
Bros. Strategy Guide (published by Brady Games).

Item Pitcher    10000 pts  Unconfirmed.  Defeat your opponent by throwing stuff 
at him!  From the Smash Bros. Strategy Guide.

Pokémon Finish  11000 pts  Finish the match with the last attack delivered via 
a Pokémon produced from a Pokéball.  (The "Wild" Pokémon on Pikachu's level 
don't count for this.  If the last hit is physically delivered by the 
characters Pikachu or Jigglypuff, despite them being Pokémon themselves, you 
don't get the points.)

Bumper Clear    11000 pts  The last hit on the last foe in the battle must have 
been due to a bumper.  There are easier bonuses to earn.

Trickster       11000 pts  During any of the onslaught matches (Yoshi, Kirby 
and Polygon) achieve a "star finish" with ALL your foes.

Shooter         12000 pts  Defeat the opposition using only projectile attacks.  
This should also be worth "Single Move."

Star Clear      12000 pts  Win the battle while invincible from a star.  The 
little sparklies that signify invincibility must still surround your character.

Booby Trap      12000 pts  The last hit on your opponent must be delivered by a 
Mine (the ones that look kind of like the Proximity Mines from Goldeneye).  
This award seems to only be granted if your player was the one to drop the 
mine.

No Damage       15000 pts  Finish without taking a hit!  ANY source of damage 
counts, including landscape features like Zebes' Acid Lava, Sector Z's ARWINGs 
and the bumper hanging over Peach Castle.  Automatically awards "Full Power" as 
well.

Mew Catch       15000 pts  Appears whenever Mew (randomly and rarely) shows up 
in a Pokéball sometime during the match.  Mew's the one who sparkles then 
leaves the board ("Mew!").  Sometimes Clefairy, another Pokémon, will imitate 
Mew; you don't get the bonus for this.

All Variations  15000 pts  Unconfirmed.  Use all of the character's moves at 
some point during a match.

Item Throw      16000 pts  Unconfirmed.  Use item throw attacks (Z+tap+A or 
R+A) above others, perhaps ONLY thrown items.

Heartthrob      17000 pts  Collect three Heart Container items during a level.  
These are relatively rare, so this is harder to get than Vegetarian.

Hawk            18000 pts  Use only aerial movies during the match.  Notice 
that if you're in a team match, bonuses such as Single Move and Hawk disregard 
the moves your allies use.

Item Strike     20000 pts  Happens when you only use items during the match.  
The most difficult part is waiting for the first items to appear.

Comet Mystic    10000 pts  Occurring most often on Samus' stage, Planet Zebes, 
reports seem to indicate that this one only shows up when you're "in the 
background," about to die from a Star Finish ("POW!  Woaaaaahhhh"), but your 
last opponent exits the match before your defeat is registered ("*ping*").  
Zebes, with its Acid Lake, is the level in which this happens most often, 
especially when the liquid gets both your character and Samus, but hits her a 
split-second before you.

Heavy Damage    28000 pts  Win the match with your damage meter reading over 
200% (NOT 300%, as reported elsewhere).

No Miss x??      5000-55000 pts
For every consecutive battle since the game's start that you win without losing 
a life, you earn a "No Miss" bonus.  The first time you die at ALL in a game  
(continues count, I'm afraid) you won't be seeing any further No Miss bonuses.  
The first time you do this you get 5000 points ("No Miss x1").  Each time after 
that it's worth 5000 points more, up to 55000 for defeating Master Hand without 
dying at all throughout the game.  (By doing that, you also get the 70000 "No 
Miss Clear" described below.)  If you manage to complete the entire game 
without dying this adds up 330000 points!  This, in addition to the ending 
bonuses, can result in a respectable score even in Very Easy mode.

Pacifist        60000 pts  Don't hit your opponent at all!  This has happened 
to me several times now against Pikachu; he just falls off the board.  When you 
get to his level, especially if you're playing Very Easy, just sit tight and 
see if he falls off (it usually happens when the computer makes an error with 
his Up-and-B save attack).  You can actually attack and still get the bonus, as 
long as you don't connect with your opponent.  Using items is also okay, as 
long as none of "your" items (those you last carried or were placed by you) 
hurts any enemy.


BONUSES ONLY GIVEN ON PARTICULAR LEVELS

Tornado Clear    3000 pts  On Link's "Hyrule Castle" stage, the enemy must have 
been dispatched by one of the tornadoes that appear on this level.

Yoshi Rainbow   50000 pts  Finish the match by defeating the Yohsis im the 
order in which they appear, just like with Kirby Ranks (see below).  Not easy.

ARWING Clear     4000 pts  The last hit on the opponent must be delivered by 
the ARWING spacecraft on Fox's Sector Z stage.

Good Friend      8000 pts  In the match against the Mario Bros., keep your 
teammate alive until the end.

True Friend     25000 pts  On the Mario Bros. level, finish with your teammate 
undamaged.

Bros. Calamity  25000 pts  Defeat one of the Mario Bros. before the other takes 
any damage.

DK Defender     10000 pts  Get your comrades through the Giant DK level.

DK Perfect      50000 pts  Complete the Giant DK stage with your allies 
unharmed.

Kirby Rank      25000 pts This comes up when you defeat the Kirbys (Kirbies?) 
in the order in which they appear.  They've all got different abilities copied, 
so you should be able to tell them apart.  Apparently the order the Kirbies 
appear doesn't change from game to game except for the last one.  The order is: 
Mario, Donkey Kong, Link, Samus, Yoshi, Fox, Pikachu and then the last one, 
which can either be a default Kirby or one of the secret characters.  Since 
both the Kirby and the Yoshi team stages have bonuses for defeating everyone in 
order, it seems fairly likely that such a bonus exists for the Fighting Polygon 
Team as well.  Can anyone find it?

Acid Clear       1500 pts  The last hit must be delivered by the acid lava on 
Samus' Planet Zebes level.

BONUSES ONLY GIVEN AFTER BONUS STAGES

No Damage       15000 pts  Complete Bonus Stage 3 (the race) without taking any 
damage.  Not even one percent!

Perfect         30000 pts  Get all the targets in Bonus Stage 1, or land on all 
the target platforms in Bonus Stage 2.

BONUSES GIVEN UPON COMPLETING A GAME

Very Easy Clear  70000 pts
     Easy Clear 140000 pts 
   Normal Clear 210000 pts
     Hard Clear 280000 pts
Very Hard Clear 350000 pts

No Miss Clear    70000 Win the game without dying to earn 70000 bonus points.  
Since this comes in addition to all the No Miss bonuses earned throughout the 
game, this is easily the single largest source of points to be found.

Speed King       40000 pts Win the game in less than twenty minutes total.  Of 
course, after doing this you get the opportunity to add Captain Falcon to the 
available characters, if he's not open already.

Speed Demon      80000 pts Win the game in a very short period of time.  One 
source rates this at eight minutes (of game time?).  This award is not 
cumulative with Speed King.

I don't promise these are at all correct.  If you know of any mistakes with 
this FAQ be sure to let me know!  (I won't post them until I can verify
them, though, just so you know!)  Do you know of any additional bonuses?
Have a correction to suggest?  Let me know at milen@gmx.net! (Notice the new 
e-mail address)


SCORES AND HIGH SCORES

At the start of a one player game, as you move the hand cursor over the 
different character portraits, at the bottom of the screen will appear the 
highest score ever earned with the character currently selected (or the highest 
score earned since the score data was last reset).  A little "Window" 
symbol/logo appears by the score if it was earned during a winning game; the 
color of the symbol corresponds to the difficulty level of that game (from blue 
for Very Easy to red for Very Hard).  To the right is a number in parenthesis.  
This number turns out to be the number of Special Bonuses earned in that game.  
Below the "High Score" listings is a similar display for "Total High Score," 
which is the sum of the high scores for all the characters, and another number 
in parenthesis, which is the total number of Special Bonuses earned in those 
highest-scoring games.

These things appear to be presented for information only; there are no known 
secret features that open up for earning high scores or obtaining large numbers 
of Special Bonuses, except this: get more than a million points during any game 
(apparently) causes, during the ending still screen, instead of the announcer 
saying "Congratulations!," to instead say "INCREDIBLE!"  There seems to be no 
other reward, even for getting over a million with all the characters.


STRATEGY
- Play on Very Easy mode unless you're *really* good (better than I am)!  The 
greatest source of both points and special bonuses, throughout the entire game, 
are the "No Miss" bonuses you get for not losing any lives.  Make it without 
dying the whole game for a total of 400 thousand points.  Winning on Very Hard 
is only worth 350 thousand (280 thousand more than winning on Very Easy).
- You get more bonuses for being a show-off than for just finishing the levels.  
You get two bonuses for winning without taking damage, a couple more for 
winning using only one move, and another one for winning quickly.  With a 
little luck it's not difficult to get bunches of points, but for some (Yoshi 
Rainbow, Kirby Ranks) you'll have to work for them.
- Master the bonus stages.  They count for a minimum of 60 thousand for getting 
perfects on both of the first two bonus stages, and another 15k for stylin' 
through the race.  That's worth an additional special bonus in both cases.
- Here's my strategy for each particular level.  As always, your mileage may 
vary.
  STAGE 1: LINK
  On Very Easy mode this one's a cinch.  Run up to Link and grab him (press R).  
With practice with each character you can discover the magic distance to stand 
from him at which you can successfully grab.  Three quick grabs and throws to 
the left and often that's it for Link!  (Note for some characters, to do a good 
throw to the left you may have to stand to the *right* of your foe.)  If he 
does make it back onto the board just grab him again.  I can do it about 90% of 
the time.  Finish the board like this and you get a whole raft of bonuses.  I 
often finish this board just shy of 64,000 points.
  STAGE 2: YOSHI TEAM
  Yoshi Rainbow sure is a tempting target to go for, but it's quite difficult 
dispatching the Yoshis in the proper order (the other colors keep getting in 
the way).  You also have to be careful to keep track of the order in which the 
Yoshis arrive.  I've done it a few times, but it's by no means a sure thing.  
Still, if you use nothing but throws (one good toss is enough to finish a 
Yoshi) and don't get hit (also a snap) you can still earn a respectable score 
for this board.
  STAGE 3: FOX
  It gets a little more difficult here, but with a little luck you'll still get 
good results.  The throw technique I suggested for Link works, but you have to 
be very good with your timing.  Smash attacks work well against Fox if you've 
gotten them down nice and proper.  Still, if a hammer appears on Fox's level I 
usually don't hesitate to go for it.  (Hammers usually give you Star Finish, 
which is an easy 10k anyway.)  In general, if you mess up on the bonuses you're 
aiming for, wait out the level a bit and see if you can finish with a Hammer, 
Pokémon, Bumper, Mine or Star, for 10,000, 11,000, 11,000, 12,000 or 12,000 
respectively.
  BONUS STAGE 1: DESTROY THE TARGETS
  Practice and a good knowledge of each character's moves are what'll help you 
here.  Try everything!
  STAGE 4: MARIO BROS.
  Sometimes you're lucky and the enemy team just falls off the board.  Don't 
count on it though.  If your friend gets through unscratched you get twenty-
five thousand, if he just survives you get eight.  Even through you get Good 
Friend most of the time this is one of the more difficult stages to milk for 
lots of points.  Often your strategy is character specific here.
  STAGE 5: PIKACHU
  If you stay away from Pikachu for long enough the computer usually suicides 
(though you'd better have good dodging skills).  So take advantage of this by 
not attacking at all!  If you stand on the helipad platform while Pikachu is 
off to the left he almost always walks right into the Pokémon door, which can 
add up to a win right there (if Pikachu is hit by computer-summoned Pokémon it 
doesn't count against Pacifist).  With practice this can be worth 60k almost 
every time!
  STAGE 6: GIANT DONKEY KONG
  If you have projectile attacks this is a prime opportunity to use them and 
get Shooter.  If you jump while shooting you can get Hawk too!  (This is done 
most often with Yoshi.)  Generally, though, if you don't take an active part in 
this battle your teammates do okay for a while, but if they die you're in 
trouble.  If you manage to get through this one with them undamaged you get DK 
Perfect, one of the few bonuses I've yet to see (supposedly it's worth eighty 
thousand!).
  BONUS STAGE 2: LAND ON THE PLATFORMS
  The platforms can be more difficult than the targets.  Sometimes you have to 
take some fairly round-about paths in order to get *every* platform (especially 
on Fox's and Captain Falcon's stages).
  STAGE 7: KIRBY TEAM
  This one usually ends up either very easy or very hard.  One good toss is 
enough to finish a Kirby, but if you don't dispose of one quickly it can tear 
you up.  Remember, you're going for points, so if you lose even one life you 
might as well give up (pause and press A+B+Z+R).  On the positive site, Kirby 
Ranks (25k) is much easier to get than Yoshi Rainbow.
  STAGE 8: SAMUS ARAN
  It may be best to just get ridda Samus quickly rather than be fancy with her.  
If you get in close quarters with her you usually take a couple of hits before 
you can get away.  And be careful whenever she's got that gun charged up!  A 
good blast from is often fatal for the weaker characters even at low damage 
levels.  If you mix it up with her fairly well you can distract her from 
worrying about the acid until it's too late.  Acid Clear's only 4k, but you 
almost always get Star Finish with it, and that's worth ten thousand.
  STAGE 9: METAL MARIO
  I've never gotten out of this one without taking damage.  While the Chrome 
Plumber is on the middle platform hit him with lots of jump attacks, but get 
out of there and back to the lower platform as soon as you can or you'll get 
hit.  Whenever he's on the lower platform use projectiles if you've got 'em.  
Items are your best source of points here.
  BONUS STAGE 3: RACE TO THE FINISH
  Every second left on the clock here is worth 500 points, not a bad sum.  The 
layout is the same with every character and it's never very hard to get to the 
end.  It's a little trickier to get through without taking damage, worth an 
extra 15k.  The places where you get hurt most often are getting through the 
bumpers (take it nice and easy, and try to run under the last one) and when 
descending a floor where an enemy is waiting for you (press Down+A while 
descending and you'll usually hit the polygon waiting for you rather than the 
other way around).
  STAGE 10: FIGHTING POLYGON TEAM
  Throw, throw, throw.  If you mess up and use an ordinary attack then take 
your time clearing out the rest, in case a high-point finishing item appears.  
On Very Easy this shouldn't be a problem but on harder levels it can be 
difficult keeping all your lives through here.
  STAGE 11: MASTER HAND
  Super Smash Bros. was designed by the creator of the Kirby games, and it 
shows here.  Master Hand is like the final boss to every Kirby game ever made!  
He has a sequence of attacks, each of which has a specific way in which you can 
avoid taking damage.  Concentrate on avoiding the attacks and get your hits in 
when you can.  It's possible to earn Full Power and No Damage on this one if 
you're careful.
  Eventually you'll wear Master Hand down and send him roaring into the vortex.  
If you're playing Very Easy, you'll now get 70,000 points, more if you're on a 
harder level.  If you didn't lose any lives through the whole game you'll also 
get No Miss Clear, worth another 70k, and if you were fast you'll probably get 
Speed King for 40 thousand.  If you paid attention and soaked up points at 
every chance there's a good chance you ended up with more than a million.  
Congratulations!

NOTES
On items in general:
* If you pick up an item, *but don't use it*, you still earn No Item.
* Using any item nullifies Single Move, even if the only move you make during a 
match is with an item.  Even if the item doesn't connect it disqualifies you.  
Health restoration items disqualify you, too.
* You never earn No Item on the Master Hand stage (where there are no items 
anyway).

On Pokémon:
Pokémon you produce...
* Do count towards Item Strike (assuming no other non-item attacks are used).
* Do count towards nullifying Pacifist.

On the Bat:
* Hits with the bat do count towards Item Strike.
* Power hits with the bat do count towards Smash Mania.

On thrown items:
* They *probably* disqualify you for Shooter.
* They do *not* disqualify you for Item Strike.
* They do disqualify you for No Item.

On Pacifist:
* Shooter, Item Strike, Smash Mania and some other bonuses which require you to 
use only one type of move in defeating your opponent are NOT earned when you 
get Pacifist (which happens when NO move connects).

On Shooter:
* Projectile attacks you can use that won't disqualify you are:
    Samus' Charge Beam (B), and probably her fire attack (tap Up+A)
    Pikachu's ground-hugging lightning-bolt (B)
    Fox's Laser (B)
    Ness' PK Fire (B) and PK Thunder (Up+B)
    Link's Boomerang (B)
    The "slash" part of Kirby's Air Slash (Up+B)
    Yoshi's launched Eggs (Up+B)
    Mario and Luigi's Fireballs (B)
    The Ray Gun's laser blasts
    The Fire Flower's fire stream 
    It's not known if a Pokémon that uses only projectile attacks counts
* You can earn both Hawk and Shooter in one match, if you launch all your 
missiles while airborne.

On Single Move:
* All item attacks disqualify you.
* All throws disqualify you.

On allies:
* Whatever they do, they don't affect special bonuses dependent on your 
actions.  If you sit back and let your comrades finish off Giant Donkey Kong, 
you *do* earn Pacifist!

My personal high scores (as of 6/2/99):
* For entertainment purposes only (please don't send in your scores or brag 
about how good you are), try to beat...
     Luigi – 1,029,417 (53)
     Mario -   950,776 (47)
      D.K. -   962,385 (51)
      Link – 1,042,469 (64)
     Samus – 1,099,626 (58)
  C.Falcon -   945,814 (44)
      Ness -   943,182 (39)
     Yoshi -   941,187 (47)
     Kirby – 1,072,327 (58)
       Fox – 1,008,225 (56)
   Pikachu – 1,139,706 (60)
Jigglypuff -   922,910 (31)

THINGS TO TRACK DOWN

* No Damage.  Supposedly if you win without taking a hit you get 300,000 
points.  In most games I'd doubt this seriously, but with Smash Bros. this just 
might be a possibility.

SPECIAL THANKS TO...
AKUMA               (Cheap Shot info, other things)
Jonathan Hensley    (Judo Warrior info)
JoshF13             (DK Perfect info)
KazeKage            (Something or other that I unfortunately misplaced)
Kasper3333          (Mystic and Comet Mystic info)
Kyle Fox            (DK Perfect)
Matt Ashbaugh       (Special Move and several other things)
Mikola Lysenko      (Mew Catch)
Mr. Strange         (Item Strike, Heartthrob)
Neal Lundberg       (Heartthrob, Item Throw, Heavy Damage lead) 
moocow121           (Smash Mania)
Nintendo Kid 21     (Yoshi Rainbow)
RealmMaster         (Mystic and Comet Mystic info)
Shade8284           (Mew Catch, Comet Mystic, Shooter, Judo Master,
                     Item Strike info)
Speedy854           (Mystic, detail on Mew Catch)
StnCold690          (DK Perfect correction, Shield Breaker info)
Theobald, Philip    (Mew Catch, Lucky 3 info)
...and uncounted others!!

Thanks to *EVERYONE* who helped!!  This FAQ is immeasurably improved through 
your contributions!  Saaaaaa-LUTE!

This document copyright 1999 by Milen. Permission to distribute granted as
long as it is not sold or published for any purpose; special, explicitly-
granted permission must be granted for this to be acceptable.